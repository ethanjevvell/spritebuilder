﻿namespace SpriteBuilder
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listBoxSprites = new System.Windows.Forms.ListBox();
            this.groupBoxSpriteProps = new System.Windows.Forms.GroupBox();
            this.groupBoxText = new System.Windows.Forms.GroupBox();
            this.numericUpDownSpriteTextScale = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxSpriteTextContent = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBoxTexture = new System.Windows.Forms.GroupBox();
            this.labelNativeSize = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxSpriteImage = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownRotation = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSpriteSizeX = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDownSpriteSizeY = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonChangeSpriteColor = new System.Windows.Forms.Button();
            this.textBoxSpriteName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDownSpriteLocationY = new System.Windows.Forms.NumericUpDown();
            this.pictureBoxSpriteColor = new System.Windows.Forms.PictureBox();
            this.numericUpDownSpriteLocationX = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxSpriteList = new System.Windows.Forms.GroupBox();
            this.buttonMoveSpriteDown = new System.Windows.Forms.Button();
            this.buttonMoveSpriteUp = new System.Windows.Forms.Button();
            this.buttonCopySprite = new System.Windows.Forms.Button();
            this.buttonRemoveSprite = new System.Windows.Forms.Button();
            this.buttonAddSprite = new System.Windows.Forms.Button();
            this.pictureBoxCanvas = new System.Windows.Forms.PictureBox();
            this.groupBoxGeneralSettings = new System.Windows.Forms.GroupBox();
            this.comboBoxScreenSurface = new System.Windows.Forms.ComboBox();
            this.comboBoxScreenBlock = new System.Windows.Forms.ComboBox();
            this.checkBoxScreenOutline = new System.Windows.Forms.CheckBox();
            this.checkBoxShowSelection = new System.Windows.Forms.CheckBox();
            this.checkBoxGuideLines = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonChangeCanvasColor = new System.Windows.Forms.Button();
            this.pictureBoxCanvasColor = new System.Windows.Forms.PictureBox();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.labelSize = new System.Windows.Forms.Label();
            this.contextMenuStripAddSprite = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemTexture = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemText = new System.Windows.Forms.ToolStripMenuItem();
            this.labelScreenOutlineSize = new System.Windows.Forms.Label();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialogGameExecutable = new System.Windows.Forms.OpenFileDialog();
            this.exportToPNGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialogPNG = new System.Windows.Forms.SaveFileDialog();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.locateSpaceEngineersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxSpriteProps.SuspendLayout();
            this.groupBoxText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteTextScale)).BeginInit();
            this.groupBoxTexture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRotation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteSizeX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteSizeY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteLocationY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpriteColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteLocationX)).BeginInit();
            this.groupBoxSpriteList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCanvas)).BeginInit();
            this.groupBoxGeneralSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCanvasColor)).BeginInit();
            this.contextMenuStripAddSprite.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxSprites
            // 
            this.listBoxSprites.FormattingEnabled = true;
            this.listBoxSprites.Location = new System.Drawing.Point(6, 14);
            this.listBoxSprites.Name = "listBoxSprites";
            this.listBoxSprites.Size = new System.Drawing.Size(208, 134);
            this.listBoxSprites.TabIndex = 0;
            this.listBoxSprites.SelectedIndexChanged += new System.EventHandler(this.listBoxSprites_SelectedIndexChanged);
            // 
            // groupBoxSpriteProps
            // 
            this.groupBoxSpriteProps.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSpriteProps.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxSpriteProps.Controls.Add(this.groupBoxText);
            this.groupBoxSpriteProps.Controls.Add(this.label7);
            this.groupBoxSpriteProps.Controls.Add(this.groupBoxTexture);
            this.groupBoxSpriteProps.Controls.Add(this.buttonChangeSpriteColor);
            this.groupBoxSpriteProps.Controls.Add(this.textBoxSpriteName);
            this.groupBoxSpriteProps.Controls.Add(this.label3);
            this.groupBoxSpriteProps.Controls.Add(this.label4);
            this.groupBoxSpriteProps.Controls.Add(this.numericUpDownSpriteLocationY);
            this.groupBoxSpriteProps.Controls.Add(this.pictureBoxSpriteColor);
            this.groupBoxSpriteProps.Controls.Add(this.numericUpDownSpriteLocationX);
            this.groupBoxSpriteProps.Controls.Add(this.label1);
            this.groupBoxSpriteProps.Location = new System.Drawing.Point(710, 253);
            this.groupBoxSpriteProps.Name = "groupBoxSpriteProps";
            this.groupBoxSpriteProps.Size = new System.Drawing.Size(223, 288);
            this.groupBoxSpriteProps.TabIndex = 1;
            this.groupBoxSpriteProps.TabStop = false;
            this.groupBoxSpriteProps.Text = "Sprite Properties";
            // 
            // groupBoxText
            // 
            this.groupBoxText.Controls.Add(this.numericUpDownSpriteTextScale);
            this.groupBoxText.Controls.Add(this.label12);
            this.groupBoxText.Controls.Add(this.label10);
            this.groupBoxText.Controls.Add(this.textBoxSpriteTextContent);
            this.groupBoxText.Location = new System.Drawing.Point(6, 270);
            this.groupBoxText.Name = "groupBoxText";
            this.groupBoxText.Size = new System.Drawing.Size(211, 106);
            this.groupBoxText.TabIndex = 9;
            this.groupBoxText.TabStop = false;
            this.groupBoxText.Text = "Text";
            // 
            // numericUpDownSpriteTextScale
            // 
            this.numericUpDownSpriteTextScale.DecimalPlaces = 2;
            this.numericUpDownSpriteTextScale.Location = new System.Drawing.Point(65, 74);
            this.numericUpDownSpriteTextScale.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownSpriteTextScale.Name = "numericUpDownSpriteTextScale";
            this.numericUpDownSpriteTextScale.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownSpriteTextScale.TabIndex = 21;
            this.numericUpDownSpriteTextScale.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSpriteTextScale.ValueChanged += new System.EventHandler(this.numericUpDownSpriteTextScale_ValueChanged);
            this.numericUpDownSpriteTextScale.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 76);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Scale";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Content";
            // 
            // textBoxSpriteTextContent
            // 
            this.textBoxSpriteTextContent.Location = new System.Drawing.Point(65, 13);
            this.textBoxSpriteTextContent.Multiline = true;
            this.textBoxSpriteTextContent.Name = "textBoxSpriteTextContent";
            this.textBoxSpriteTextContent.Size = new System.Drawing.Size(106, 55);
            this.textBoxSpriteTextContent.TabIndex = 10;
            this.textBoxSpriteTextContent.Text = "Text";
            this.textBoxSpriteTextContent.TextChanged += new System.EventHandler(this.textBoxSpriteTextContent_TextChanged);
            this.textBoxSpriteTextContent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Position Y";
            // 
            // groupBoxTexture
            // 
            this.groupBoxTexture.Controls.Add(this.labelNativeSize);
            this.groupBoxTexture.Controls.Add(this.label8);
            this.groupBoxTexture.Controls.Add(this.comboBoxSpriteImage);
            this.groupBoxTexture.Controls.Add(this.label2);
            this.groupBoxTexture.Controls.Add(this.numericUpDownRotation);
            this.groupBoxTexture.Controls.Add(this.numericUpDownSpriteSizeX);
            this.groupBoxTexture.Controls.Add(this.label11);
            this.groupBoxTexture.Controls.Add(this.numericUpDownSpriteSizeY);
            this.groupBoxTexture.Controls.Add(this.label6);
            this.groupBoxTexture.Location = new System.Drawing.Point(6, 131);
            this.groupBoxTexture.Name = "groupBoxTexture";
            this.groupBoxTexture.Size = new System.Drawing.Size(211, 133);
            this.groupBoxTexture.TabIndex = 10;
            this.groupBoxTexture.TabStop = false;
            this.groupBoxTexture.Text = "Texture";
            // 
            // labelNativeSize
            // 
            this.labelNativeSize.AutoSize = true;
            this.labelNativeSize.Location = new System.Drawing.Point(130, 46);
            this.labelNativeSize.Name = "labelNativeSize";
            this.labelNativeSize.Size = new System.Drawing.Size(64, 26);
            this.labelNativeSize.TabIndex = 22;
            this.labelNativeSize.Text = "Native Size:\r\nN/A";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Height";
            // 
            // comboBoxSpriteImage
            // 
            this.comboBoxSpriteImage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSpriteImage.FormattingEnabled = true;
            this.comboBoxSpriteImage.Location = new System.Drawing.Point(65, 13);
            this.comboBoxSpriteImage.Name = "comboBoxSpriteImage";
            this.comboBoxSpriteImage.Size = new System.Drawing.Size(140, 21);
            this.comboBoxSpriteImage.TabIndex = 14;
            this.comboBoxSpriteImage.SelectedIndexChanged += new System.EventHandler(this.comboBoxSpriteImage_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Width";
            // 
            // numericUpDownRotation
            // 
            this.numericUpDownRotation.Location = new System.Drawing.Point(65, 44);
            this.numericUpDownRotation.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownRotation.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numericUpDownRotation.Name = "numericUpDownRotation";
            this.numericUpDownRotation.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownRotation.TabIndex = 20;
            this.numericUpDownRotation.ValueChanged += new System.EventHandler(this.numericUpDownRotation_ValueChanged);
            this.numericUpDownRotation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // numericUpDownSpriteSizeX
            // 
            this.numericUpDownSpriteSizeX.Location = new System.Drawing.Point(65, 74);
            this.numericUpDownSpriteSizeX.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeX.Name = "numericUpDownSpriteSizeX";
            this.numericUpDownSpriteSizeX.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownSpriteSizeX.TabIndex = 4;
            this.numericUpDownSpriteSizeX.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeX.ValueChanged += new System.EventHandler(this.numericUpDownSpriteSizeX_ValueChanged);
            this.numericUpDownSpriteSizeX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Rotation";
            // 
            // numericUpDownSpriteSizeY
            // 
            this.numericUpDownSpriteSizeY.Location = new System.Drawing.Point(65, 104);
            this.numericUpDownSpriteSizeY.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeY.Name = "numericUpDownSpriteSizeY";
            this.numericUpDownSpriteSizeY.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownSpriteSizeY.TabIndex = 5;
            this.numericUpDownSpriteSizeY.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeY.ValueChanged += new System.EventHandler(this.numericUpDownSpriteSizeY_ValueChanged);
            this.numericUpDownSpriteSizeY.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Sprite";
            // 
            // buttonChangeSpriteColor
            // 
            this.buttonChangeSpriteColor.Location = new System.Drawing.Point(105, 100);
            this.buttonChangeSpriteColor.Name = "buttonChangeSpriteColor";
            this.buttonChangeSpriteColor.Size = new System.Drawing.Size(87, 25);
            this.buttonChangeSpriteColor.TabIndex = 12;
            this.buttonChangeSpriteColor.Text = "Change Color";
            this.buttonChangeSpriteColor.UseVisualStyleBackColor = true;
            this.buttonChangeSpriteColor.Click += new System.EventHandler(this.buttonChangeSpriteColor_Click);
            // 
            // textBoxSpriteName
            // 
            this.textBoxSpriteName.Location = new System.Drawing.Point(71, 13);
            this.textBoxSpriteName.Name = "textBoxSpriteName";
            this.textBoxSpriteName.Size = new System.Drawing.Size(121, 20);
            this.textBoxSpriteName.TabIndex = 9;
            this.textBoxSpriteName.TextChanged += new System.EventHandler(this.textBoxSpriteName_TextChanged);
            this.textBoxSpriteName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Color";
            // 
            // numericUpDownSpriteLocationY
            // 
            this.numericUpDownSpriteLocationY.Location = new System.Drawing.Point(71, 76);
            this.numericUpDownSpriteLocationY.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownSpriteLocationY.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numericUpDownSpriteLocationY.Name = "numericUpDownSpriteLocationY";
            this.numericUpDownSpriteLocationY.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownSpriteLocationY.TabIndex = 7;
            this.numericUpDownSpriteLocationY.ValueChanged += new System.EventHandler(this.numericUpDownSpriteLocationY_ValueChanged);
            this.numericUpDownSpriteLocationY.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // pictureBoxSpriteColor
            // 
            this.pictureBoxSpriteColor.BackColor = System.Drawing.Color.Black;
            this.pictureBoxSpriteColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSpriteColor.Location = new System.Drawing.Point(71, 100);
            this.pictureBoxSpriteColor.Name = "pictureBoxSpriteColor";
            this.pictureBoxSpriteColor.Size = new System.Drawing.Size(28, 25);
            this.pictureBoxSpriteColor.TabIndex = 10;
            this.pictureBoxSpriteColor.TabStop = false;
            // 
            // numericUpDownSpriteLocationX
            // 
            this.numericUpDownSpriteLocationX.Location = new System.Drawing.Point(71, 44);
            this.numericUpDownSpriteLocationX.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownSpriteLocationX.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numericUpDownSpriteLocationX.Name = "numericUpDownSpriteLocationX";
            this.numericUpDownSpriteLocationX.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownSpriteLocationX.TabIndex = 6;
            this.numericUpDownSpriteLocationX.ValueChanged += new System.EventHandler(this.numericUpDownSpriteLocationX_ValueChanged);
            this.numericUpDownSpriteLocationX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Position X";
            // 
            // groupBoxSpriteList
            // 
            this.groupBoxSpriteList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSpriteList.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxSpriteList.Controls.Add(this.buttonMoveSpriteDown);
            this.groupBoxSpriteList.Controls.Add(this.buttonMoveSpriteUp);
            this.groupBoxSpriteList.Controls.Add(this.buttonCopySprite);
            this.groupBoxSpriteList.Controls.Add(this.buttonRemoveSprite);
            this.groupBoxSpriteList.Controls.Add(this.buttonAddSprite);
            this.groupBoxSpriteList.Controls.Add(this.listBoxSprites);
            this.groupBoxSpriteList.Location = new System.Drawing.Point(710, 29);
            this.groupBoxSpriteList.Name = "groupBoxSpriteList";
            this.groupBoxSpriteList.Size = new System.Drawing.Size(223, 218);
            this.groupBoxSpriteList.TabIndex = 2;
            this.groupBoxSpriteList.TabStop = false;
            this.groupBoxSpriteList.Text = "Sprite List";
            // 
            // buttonMoveSpriteDown
            // 
            this.buttonMoveSpriteDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMoveSpriteDown.Location = new System.Drawing.Point(71, 151);
            this.buttonMoveSpriteDown.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMoveSpriteDown.Name = "buttonMoveSpriteDown";
            this.buttonMoveSpriteDown.Size = new System.Drawing.Size(37, 27);
            this.buttonMoveSpriteDown.TabIndex = 5;
            this.buttonMoveSpriteDown.Text = "↓";
            this.buttonMoveSpriteDown.UseVisualStyleBackColor = true;
            this.buttonMoveSpriteDown.Click += new System.EventHandler(this.buttonMoveSpriteDown_Click);
            // 
            // buttonMoveSpriteUp
            // 
            this.buttonMoveSpriteUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMoveSpriteUp.Location = new System.Drawing.Point(33, 151);
            this.buttonMoveSpriteUp.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMoveSpriteUp.Name = "buttonMoveSpriteUp";
            this.buttonMoveSpriteUp.Size = new System.Drawing.Size(37, 27);
            this.buttonMoveSpriteUp.TabIndex = 4;
            this.buttonMoveSpriteUp.Text = "↑";
            this.buttonMoveSpriteUp.UseVisualStyleBackColor = true;
            this.buttonMoveSpriteUp.Click += new System.EventHandler(this.buttonMoveSpriteUp_Click);
            // 
            // buttonCopySprite
            // 
            this.buttonCopySprite.Location = new System.Drawing.Point(114, 152);
            this.buttonCopySprite.Name = "buttonCopySprite";
            this.buttonCopySprite.Size = new System.Drawing.Size(75, 27);
            this.buttonCopySprite.TabIndex = 3;
            this.buttonCopySprite.Text = "Copy";
            this.buttonCopySprite.UseVisualStyleBackColor = true;
            this.buttonCopySprite.Click += new System.EventHandler(this.buttonCopySprite_Click);
            // 
            // buttonRemoveSprite
            // 
            this.buttonRemoveSprite.Location = new System.Drawing.Point(114, 185);
            this.buttonRemoveSprite.Name = "buttonRemoveSprite";
            this.buttonRemoveSprite.Size = new System.Drawing.Size(75, 27);
            this.buttonRemoveSprite.TabIndex = 2;
            this.buttonRemoveSprite.Text = "Remove";
            this.buttonRemoveSprite.UseVisualStyleBackColor = true;
            this.buttonRemoveSprite.Click += new System.EventHandler(this.buttonRemoveSprite_Click);
            // 
            // buttonAddSprite
            // 
            this.buttonAddSprite.Location = new System.Drawing.Point(33, 185);
            this.buttonAddSprite.Name = "buttonAddSprite";
            this.buttonAddSprite.Size = new System.Drawing.Size(75, 27);
            this.buttonAddSprite.TabIndex = 1;
            this.buttonAddSprite.Text = "Add";
            this.buttonAddSprite.UseVisualStyleBackColor = true;
            this.buttonAddSprite.Click += new System.EventHandler(this.buttonAddSprite_Click);
            // 
            // pictureBoxCanvas
            // 
            this.pictureBoxCanvas.BackColor = System.Drawing.Color.Black;
            this.pictureBoxCanvas.Location = new System.Drawing.Point(192, 29);
            this.pictureBoxCanvas.Name = "pictureBoxCanvas";
            this.pictureBoxCanvas.Size = new System.Drawing.Size(512, 512);
            this.pictureBoxCanvas.TabIndex = 3;
            this.pictureBoxCanvas.TabStop = false;
            this.pictureBoxCanvas.Click += new System.EventHandler(this.pictureBoxCanvas_Click);
            this.pictureBoxCanvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCanvas_MouseDown);
            this.pictureBoxCanvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCanvas_MouseMove);
            this.pictureBoxCanvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCanvas_MouseUp);
            // 
            // groupBoxGeneralSettings
            // 
            this.groupBoxGeneralSettings.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxGeneralSettings.Controls.Add(this.comboBoxScreenSurface);
            this.groupBoxGeneralSettings.Controls.Add(this.comboBoxScreenBlock);
            this.groupBoxGeneralSettings.Controls.Add(this.checkBoxScreenOutline);
            this.groupBoxGeneralSettings.Controls.Add(this.checkBoxShowSelection);
            this.groupBoxGeneralSettings.Controls.Add(this.checkBoxGuideLines);
            this.groupBoxGeneralSettings.Controls.Add(this.label5);
            this.groupBoxGeneralSettings.Controls.Add(this.buttonChangeCanvasColor);
            this.groupBoxGeneralSettings.Controls.Add(this.pictureBoxCanvasColor);
            this.groupBoxGeneralSettings.Location = new System.Drawing.Point(12, 29);
            this.groupBoxGeneralSettings.Name = "groupBoxGeneralSettings";
            this.groupBoxGeneralSettings.Size = new System.Drawing.Size(176, 187);
            this.groupBoxGeneralSettings.TabIndex = 4;
            this.groupBoxGeneralSettings.TabStop = false;
            this.groupBoxGeneralSettings.Text = "Canvas Settings";
            // 
            // comboBoxScreenSurface
            // 
            this.comboBoxScreenSurface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxScreenSurface.FormattingEnabled = true;
            this.comboBoxScreenSurface.Location = new System.Drawing.Point(21, 155);
            this.comboBoxScreenSurface.Name = "comboBoxScreenSurface";
            this.comboBoxScreenSurface.Size = new System.Drawing.Size(149, 21);
            this.comboBoxScreenSurface.TabIndex = 19;
            this.comboBoxScreenSurface.SelectedIndexChanged += new System.EventHandler(this.comboBoxScreenSurface_SelectedIndexChanged);
            // 
            // comboBoxScreenBlock
            // 
            this.comboBoxScreenBlock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxScreenBlock.FormattingEnabled = true;
            this.comboBoxScreenBlock.Location = new System.Drawing.Point(21, 128);
            this.comboBoxScreenBlock.Name = "comboBoxScreenBlock";
            this.comboBoxScreenBlock.Size = new System.Drawing.Size(149, 21);
            this.comboBoxScreenBlock.TabIndex = 18;
            this.comboBoxScreenBlock.SelectedIndexChanged += new System.EventHandler(this.comboBoxScreenBlock_SelectedIndexChanged);
            // 
            // checkBoxScreenOutline
            // 
            this.checkBoxScreenOutline.AutoSize = true;
            this.checkBoxScreenOutline.Location = new System.Drawing.Point(6, 109);
            this.checkBoxScreenOutline.Name = "checkBoxScreenOutline";
            this.checkBoxScreenOutline.Size = new System.Drawing.Size(126, 17);
            this.checkBoxScreenOutline.TabIndex = 17;
            this.checkBoxScreenOutline.Text = "Show Screen Outline";
            this.checkBoxScreenOutline.UseVisualStyleBackColor = true;
            this.checkBoxScreenOutline.CheckedChanged += new System.EventHandler(this.checkBoxScreenOutline_CheckedChanged);
            // 
            // checkBoxShowSelection
            // 
            this.checkBoxShowSelection.AutoSize = true;
            this.checkBoxShowSelection.Checked = true;
            this.checkBoxShowSelection.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxShowSelection.Location = new System.Drawing.Point(6, 86);
            this.checkBoxShowSelection.Name = "checkBoxShowSelection";
            this.checkBoxShowSelection.Size = new System.Drawing.Size(121, 17);
            this.checkBoxShowSelection.TabIndex = 16;
            this.checkBoxShowSelection.Text = "Show Selection Box";
            this.checkBoxShowSelection.UseVisualStyleBackColor = true;
            this.checkBoxShowSelection.CheckedChanged += new System.EventHandler(this.checkBoxShowSelection_CheckedChanged);
            // 
            // checkBoxGuideLines
            // 
            this.checkBoxGuideLines.AutoSize = true;
            this.checkBoxGuideLines.Location = new System.Drawing.Point(6, 63);
            this.checkBoxGuideLines.Name = "checkBoxGuideLines";
            this.checkBoxGuideLines.Size = new System.Drawing.Size(112, 17);
            this.checkBoxGuideLines.TabIndex = 15;
            this.checkBoxGuideLines.Text = "Show Guide Lines";
            this.checkBoxGuideLines.UseVisualStyleBackColor = true;
            this.checkBoxGuideLines.CheckedChanged += new System.EventHandler(this.checkBoxGuideLines_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Background Color";
            // 
            // buttonChangeCanvasColor
            // 
            this.buttonChangeCanvasColor.Location = new System.Drawing.Point(40, 32);
            this.buttonChangeCanvasColor.Name = "buttonChangeCanvasColor";
            this.buttonChangeCanvasColor.Size = new System.Drawing.Size(119, 25);
            this.buttonChangeCanvasColor.TabIndex = 13;
            this.buttonChangeCanvasColor.Text = "Change Color";
            this.buttonChangeCanvasColor.UseVisualStyleBackColor = true;
            this.buttonChangeCanvasColor.Click += new System.EventHandler(this.buttonChangeCanvasColor_Click);
            // 
            // pictureBoxCanvasColor
            // 
            this.pictureBoxCanvasColor.BackColor = System.Drawing.Color.Black;
            this.pictureBoxCanvasColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxCanvasColor.Location = new System.Drawing.Point(6, 32);
            this.pictureBoxCanvasColor.Name = "pictureBoxCanvasColor";
            this.pictureBoxCanvasColor.Size = new System.Drawing.Size(28, 25);
            this.pictureBoxCanvasColor.TabIndex = 0;
            this.pictureBoxCanvasColor.TabStop = false;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.FileName = "NewProject.xml";
            this.saveFileDialog.Filter = "XML files|*.xml|All files|*.*";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "XML files|*.xml|All files|*.*";
            // 
            // labelSize
            // 
            this.labelSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSize.AutoSize = true;
            this.labelSize.Location = new System.Drawing.Point(30, 528);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(117, 13);
            this.labelSize.TabIndex = 6;
            this.labelSize.Text = "Canvas size: 512 x 512";
            // 
            // contextMenuStripAddSprite
            // 
            this.contextMenuStripAddSprite.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemTexture,
            this.toolStripMenuItemText});
            this.contextMenuStripAddSprite.Name = "contextMenuStripAddSprite";
            this.contextMenuStripAddSprite.Size = new System.Drawing.Size(113, 48);
            this.contextMenuStripAddSprite.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStripAddSprite_ItemClicked);
            // 
            // toolStripMenuItemTexture
            // 
            this.toolStripMenuItemTexture.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripMenuItemTexture.Name = "toolStripMenuItemTexture";
            this.toolStripMenuItemTexture.Size = new System.Drawing.Size(112, 22);
            this.toolStripMenuItemTexture.Text = "Texture";
            this.toolStripMenuItemTexture.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripMenuItemText
            // 
            this.toolStripMenuItemText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripMenuItemText.Name = "toolStripMenuItemText";
            this.toolStripMenuItemText.Size = new System.Drawing.Size(112, 22);
            this.toolStripMenuItemText.Text = "Text";
            this.toolStripMenuItemText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelScreenOutlineSize
            // 
            this.labelScreenOutlineSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelScreenOutlineSize.AutoSize = true;
            this.labelScreenOutlineSize.Location = new System.Drawing.Point(17, 509);
            this.labelScreenOutlineSize.Name = "labelScreenOutlineSize";
            this.labelScreenOutlineSize.Size = new System.Drawing.Size(149, 13);
            this.labelScreenOutlineSize.TabIndex = 7;
            this.labelScreenOutlineSize.Text = "Screen outline size: 512 x 512";
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(936, 24);
            this.menuStrip.TabIndex = 8;
            this.menuStrip.Text = "menuStrip";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.exportToolStripMenuItem,
            this.exportToPNGToolStripMenuItem,
            this.toolStripSeparator2,
            this.locateSpaceEngineersToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.optionsToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.newToolStripMenuItem.Text = "New                                                          Ctrl + N";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.openToolStripMenuItem.Text = "Open                                                        Ctrl + O";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.saveToolStripMenuItem.Text = "Save                                                          Ctrl + S";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.saveAsToolStripMenuItem.Text = "Save As                                        Ctrl + Shift + S";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.buttonSaveAs_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.exportToolStripMenuItem.Text = "Export                                                       Ctrl + E";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // exportToPNGToolStripMenuItem
            // 
            this.exportToPNGToolStripMenuItem.Name = "exportToPNGToolStripMenuItem";
            this.exportToPNGToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.exportToPNGToolStripMenuItem.Text = "Export to PNG";
            this.exportToPNGToolStripMenuItem.Click += new System.EventHandler(this.exportToPNGToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(251, 6);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(311, 6);
            // 
            // locateSpaceEngineersToolStripMenuItem
            // 
            this.locateSpaceEngineersToolStripMenuItem.Name = "locateSpaceEngineersToolStripMenuItem";
            this.locateSpaceEngineersToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.locateSpaceEngineersToolStripMenuItem.Text = "Locate Space Engineers ";
            this.locateSpaceEngineersToolStripMenuItem.Click += new System.EventHandler(this.locateGameExecutableToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.ToolTipText = "Open Sprite Builder wiki in browser";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(936, 545);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.labelScreenOutlineSize);
            this.Controls.Add(this.labelSize);
            this.Controls.Add(this.groupBoxGeneralSettings);
            this.Controls.Add(this.pictureBoxCanvas);
            this.Controls.Add(this.groupBoxSpriteList);
            this.Controls.Add(this.groupBoxSpriteProps);
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(560, 584);
            this.Name = "MainForm";
            this.Text = "Sprite Builder";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.groupBoxSpriteProps.ResumeLayout(false);
            this.groupBoxSpriteProps.PerformLayout();
            this.groupBoxText.ResumeLayout(false);
            this.groupBoxText.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteTextScale)).EndInit();
            this.groupBoxTexture.ResumeLayout(false);
            this.groupBoxTexture.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRotation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteSizeX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteSizeY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteLocationY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpriteColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteLocationX)).EndInit();
            this.groupBoxSpriteList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCanvas)).EndInit();
            this.groupBoxGeneralSettings.ResumeLayout(false);
            this.groupBoxGeneralSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCanvasColor)).EndInit();
            this.contextMenuStripAddSprite.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxSprites;
        private System.Windows.Forms.GroupBox groupBoxSpriteProps;
        private System.Windows.Forms.GroupBox groupBoxSpriteList;
        private System.Windows.Forms.Button buttonCopySprite;
        private System.Windows.Forms.Button buttonRemoveSprite;
        private System.Windows.Forms.Button buttonAddSprite;
        private System.Windows.Forms.PictureBox pictureBoxCanvas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxGeneralSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBoxCanvasColor;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.PictureBox pictureBoxSpriteColor;
        private System.Windows.Forms.TextBox textBoxSpriteName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownSpriteLocationY;
        private System.Windows.Forms.NumericUpDown numericUpDownSpriteLocationX;
        private System.Windows.Forms.NumericUpDown numericUpDownSpriteSizeY;
        private System.Windows.Forms.NumericUpDown numericUpDownSpriteSizeX;
        private System.Windows.Forms.Button buttonChangeSpriteColor;
        private System.Windows.Forms.Button buttonChangeCanvasColor;
        private System.Windows.Forms.CheckBox checkBoxGuideLines;
        private System.Windows.Forms.ComboBox comboBoxSpriteImage;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonMoveSpriteUp;
        private System.Windows.Forms.Button buttonMoveSpriteDown;
        private System.Windows.Forms.NumericUpDown numericUpDownRotation;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxShowSelection;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripAddSprite;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemTexture;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemText;
        private System.Windows.Forms.GroupBox groupBoxText;
        private System.Windows.Forms.NumericUpDown numericUpDownSpriteTextScale;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxSpriteTextContent;
        private System.Windows.Forms.GroupBox groupBoxTexture;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBoxScreenOutline;
        private System.Windows.Forms.ComboBox comboBoxScreenSurface;
        private System.Windows.Forms.ComboBox comboBoxScreenBlock;
        private System.Windows.Forms.Label labelScreenOutlineSize;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialogGameExecutable;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.Label labelNativeSize;
        private System.Windows.Forms.ToolStripMenuItem exportToPNGToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialogPNG;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem locateSpaceEngineersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
    }
}

