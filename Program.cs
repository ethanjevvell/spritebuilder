﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Whiplash.Utils;

namespace SpriteBuilder
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainForm mainForm = new MainForm();
            try
            {
                Application.Run(mainForm);
            }
            catch (Exception e)
            {
                if (Logger.Default != null)
                {
                    Logger.Default.WriteLine(e.ToString(), Logger.Severity.Error);
                    Logger.Default.WriteLine($"Dumping current project to XML:\n{mainForm.SaveToXml()}", Logger.Severity.Warning);
                    Logger.Default.Close();

                    MessageBox.Show(
                        "A critical error has crashed the application!\n"
                        + "Please provide the file 'SpriteBuilder.log' to Whiplash141 in " 
                        + "a bug report.",
                       "APPLICATION CRASHED", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
                throw e;
            }
        }
    }
}
