﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpriteBuilder
{
    public class SpriteContent
    {
        public SerializableColor BackgroundColor;

        public SpriteInfo[] Sprites;

        public SpriteContent()
        { }

        public SpriteContent(Color backgroundColor, List<SpriteInfo> sprites)
        {
            BackgroundColor = new SerializableColor(backgroundColor);
            Sprites = sprites.ToArray();
        }
    }
}
