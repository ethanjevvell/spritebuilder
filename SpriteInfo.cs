﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Drawing;

namespace SpriteBuilder
{
    public enum SpriteType { TEXTURE, TEXT }

    public class SpriteInfo
    {
        public string Name;
        public string SpriteType;
        public Vector2 Position;
        public Vector2 Size;
        public SerializableColor Color;
        public float Rotation;
        public SpriteType Type;
        public string Font;

        public SpriteInfo()
        {
            Name = "";
            SpriteType = "";
            Position = new Vector2(0, 0);
            Size = new Vector2(100, 100);
            Color = new SerializableColor(System.Drawing.Color.White);
            Rotation = 0;
            Type = SpriteBuilder.SpriteType.TEXTURE;
            Font = "Debug";
        }

        public SpriteInfo(string name, string data, SpriteType type)
        {
            Name = name;
            SpriteType = data;
            Position = new Vector2(0, 0);
            Size = new Vector2(100, 100);
            Color = new SerializableColor(System.Drawing.Color.White);
            Rotation = type == SpriteBuilder.SpriteType.TEXTURE ? 0 : 1;
            Type = type;
            Font = "Debug";
        }
    }  
}
